package ba.etf.rma21.projekat.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(primaryKeys = ["id", "idKviza"])
data class GrupaKviz (
    val id: Int,
    val idKviza: Int
)