package ba.etf.rma21.projekat.data.models

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey


@Entity
data class Grupa(
    @PrimaryKey val id: Int,
    val naziv: String,
    val IdPredmeta: Int,
    val nazivPredmeta: String,
    val idStudenta: Int,
)