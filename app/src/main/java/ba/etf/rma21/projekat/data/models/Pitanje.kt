package ba.etf.rma21.projekat.data.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Pitanje(
        @PrimaryKey val id: Int,
        val naziv: String,
        val tekstPitanja: String,
        val opcije: String,
        val tacan: Int,
        val nazivKviza: String? = null,
        val nazivPredmeta: String? = null,
        var odgovor: Int? = null,
        var idKviza: Int = 0
): Parcelable {
}
