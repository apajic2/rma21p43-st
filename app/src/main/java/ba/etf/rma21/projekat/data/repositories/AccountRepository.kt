package ba.etf.rma21.projekat.data.repositories

import android.annotation.SuppressLint
import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Student
import ba.etf.rma21.projekat.data.response.StudentResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@SuppressLint("StaticFieldLeak")
object AccountRepository {
    var acHash: String = "c5e265d5-0684-4f23-9572-95e2131ffe40"

    lateinit var _context: Context

    fun setContext(context: Context) {
        _context = context
    }

    fun postaviHash(acHash: String): Boolean {
        this.acHash = acHash
        return true
    }

    fun getHash(): String = acHash

    suspend fun dajStudenta(): Student {
        return AppDatabase.getInstance(_context).accountDao().dajStudenta()
    }


}