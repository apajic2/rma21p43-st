package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.response.KvizResponse
import ba.etf.rma21.projekat.data.response.PredmetResponse
import retrofit2.http.GET
import retrofit2.http.Path

@Dao
interface PredmetDao {
    @Query("SELECT * FROM predmet")
    suspend fun dajSvePredmete(): List<Predmet>

    @Query("SELECT * FROM predmet WHERE id LIKE :id")
    suspend fun dajPredmet(@Path("id") id: Int): Predmet

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun dodajPredmete(predmet: List<Predmet>)
}