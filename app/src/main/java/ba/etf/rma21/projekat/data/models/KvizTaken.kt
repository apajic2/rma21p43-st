package ba.etf.rma21.projekat.data.models

import androidx.room.Entity
import java.util.*

@Entity
data class KvizTaken(
    val id: Int,
    val student: String,
    val osvojeniBodovi: Int,
    val datumRada: String
)