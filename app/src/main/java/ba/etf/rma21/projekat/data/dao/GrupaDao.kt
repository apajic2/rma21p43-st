package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.GrupaKviz
import ba.etf.rma21.projekat.data.models.GrupaSaKvizovima
import ba.etf.rma21.projekat.data.response.DodajUGrupuResponse
import ba.etf.rma21.projekat.data.response.GrupaResponse
import ba.etf.rma21.projekat.data.response.KvizResponse
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

@Dao
interface GrupaDao {
    @Query("SELECT * FROM grupa")
    suspend fun dajSveGrupe(): List<Grupa>

    @Query("SELECT * FROM grupa WHERE id LIKE :idKviza")
    suspend fun dajGrupe(@Path("id") idKviza: Int): List<Grupa>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun dodajGrupe(grupa: List<Grupa>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun dodajGrupaKviz(grupaKviz: List<GrupaKviz>)

    @Transaction
    @Query("SELECT * FROM grupa WHERE :idGrupe LIKE id")
    suspend fun dajGrupeSaKvizovima(idGrupe: Int): List<GrupaSaKvizovima>

}