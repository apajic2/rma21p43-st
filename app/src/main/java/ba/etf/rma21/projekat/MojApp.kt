package ba.etf.rma21.projekat

import android.app.Application
import ba.etf.rma21.projekat.data.repositories.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MojApp : Application() {

    override fun onCreate() {
        super.onCreate()
        DBRepository._context = this
        KvizRepository.context = this
        OdgovorRepository._context = this
        AccountRepository._context = this
        PitanjeKvizRepository.context = this
        TakeKvizRepository.context = this
        PredmetIGrupaRepository._context = this

        GlobalScope.launch {
            DBRepository.updateNow()
        }

    }
}