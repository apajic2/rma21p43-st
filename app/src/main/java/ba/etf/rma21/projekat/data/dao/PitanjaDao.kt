package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.response.PitanjeResponse

@Dao
interface PitanjaDao {

    @Query("SELECT * FROM pitanje WHERE :id LIKE idKviza")
    suspend fun getPitanja(id: Int): List<Pitanje>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun dodajPitanja(pitanja: List<Pitanje>)
}