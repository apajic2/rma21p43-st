package ba.etf.rma21.projekat.data.repositories

import android.annotation.SuppressLint
import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Kviz

@SuppressLint("StaticFieldLeak")
object KvizRepository {

    lateinit var context: Context

    suspend fun mojiKvizovi(): List<Kviz> {
        return getUpisani()
    }

    suspend fun getMyKvizes(): List<Kviz> {
        return mojiKvizovi().sortedBy { it.datumPocetka }
    }

    suspend fun getAll(): List<Kviz> {
        return ApiConfig.dajKvizApi().dajSveKvizove().map { kvizResponse ->
            Kviz(
                kvizResponse.id,
                kvizResponse.naziv,
                "",
                kvizResponse.datumPocetak ?: "",
                kvizResponse.datumKraj ?: "",
                kvizResponse.trajanje,
            )
        }
    }

    suspend fun getDone(): List<Kviz> {
        return mojiKvizovi().sortedBy {
            it.datumPocetka
        }
    }

    suspend fun getFuture(): List<Kviz> {
        return mojiKvizovi().sortedBy { it.datumPocetka }

    }

    suspend fun getNotTaken(): List<Kviz> {
        return mojiKvizovi().sortedBy { it.datumPocetka }
    }

    suspend fun getUpisani(): List<Kviz> {
        val kvizovi = mutableSetOf<Kviz>()
        PredmetIGrupaRepository.getUpisaneGrupe().forEach {
            val grupe = AppDatabase.getInstance(context).grupaDao().dajGrupeSaKvizovima(it.id)
            grupe.forEach {  grupeSaKvizovima ->
                kvizovi.addAll(grupeSaKvizovima.kvizovi)
            }
        }
        return kvizovi.toList()
    }

}