package ba.etf.rma21.projekat.data.models

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Junction
import androidx.room.Relation


data class GrupaSaKvizovima (
    @Embedded val grupa: Grupa,
    @Relation(
        parentColumn = "id",
        entityColumn = "id",
        associateBy = Junction(GrupaKviz::class)
    )
    val kvizovi: List<Kviz>
)