package ba.etf.rma21.projekat.data.repositories

import android.annotation.SuppressLint
import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.response.PitanjeResponse


@SuppressLint("StaticFieldLeak")
object PitanjeKvizRepository {

    suspend fun getPitanja(idKviza: Int): List<Pitanje> {
        return AppDatabase.getInstance(context).pitanjaDao().getPitanja(idKviza)
    }


    lateinit var context: Context
}