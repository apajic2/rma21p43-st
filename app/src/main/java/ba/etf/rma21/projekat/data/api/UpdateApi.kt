package ba.etf.rma21.projekat.data.api

import ba.etf.rma21.projekat.data.response.KvizResponse
import ba.etf.rma21.projekat.data.response.UpdateResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface UpdateApi {

    @GET("/account/{id}/lastUpdate")
    suspend fun dajUpdateStatus(@Path("id") id: String, @Query("date") date: String): UpdateResponse
}