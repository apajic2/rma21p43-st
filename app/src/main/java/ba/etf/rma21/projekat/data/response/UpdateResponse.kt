package ba.etf.rma21.projekat.data.response


import androidx.room.Entity
import com.google.gson.annotations.SerializedName
data class UpdateResponse(
    @SerializedName("changed")
    val changed: String
)