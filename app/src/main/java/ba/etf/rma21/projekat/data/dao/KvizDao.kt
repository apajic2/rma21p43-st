package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.response.KvizResponse
import retrofit2.http.GET
import retrofit2.http.Path

@Dao
interface KvizDao {

    @Query("SELECT * FROM kviz")
    suspend fun dajSveKvizove(): List<Kviz>

    @Query("SELECT * FROM kviz WHERE id LIKE :idKviza")
    suspend fun dajKvizoveId(idKviza: Int): Kviz


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun dodajSveKvizove(kviz: List<Kviz>)
}