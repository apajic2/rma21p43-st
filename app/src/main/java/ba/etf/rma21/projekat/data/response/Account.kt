package ba.etf.rma21.projekat.data.response
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Account(
    @PrimaryKey val acHash: String,
    val lastUpdate: String
)