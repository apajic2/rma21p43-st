# RMA - Projekat 2021
Ovo je početni android projekat za projekat na predmetu Razvoj mobilnih aplikacija

Pri preuzimanju ovog repozitorija editujte ovaj Readme tako da sadrži vaš podatke.

Ne mijenjajte naziv aplikacije niti početnih paketa.


| Ime i prezime  | Index  | Grupa     |
|----------------|--------|-----------|
| Arzija Pajić     | 43-ST  | UTO 15:00 |

&nbsp;

Vaš projekat postavite na privatni repozitorij na Bitbucket-u sa nazivom RMA21PNNNNN, gdje je _NNNNN_ broj indexa. Projekat podijelite sa asistentima.
NAPOMENA!U testovima (i treće i četvrte spirale) kada se poziva response.body() - kod mene se mora pozivati kao response.body, tako da sam ja zagrade obrisala,
ostalo je sve ostalo isto.
