package ba.etf.rma21.projekat.data.models

import androidx.room.Entity

data class PitanjeKviz(
    val nazivPitanja: String,
    val kviz: String
)
