package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.Student
import ba.etf.rma21.projekat.data.response.PitanjeResponse

@Dao
interface AccountDao {

    @Query("SELECT * FROM student")
    suspend fun dajStudenta(): Student

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun dodajStudenta(student: Student)

    @Query("DELETE FROM student")
    suspend fun obrisiSve();
}