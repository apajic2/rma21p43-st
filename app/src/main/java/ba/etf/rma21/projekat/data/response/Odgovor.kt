package ba.etf.rma21.projekat.data.response
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Odgovor(
    @SerializedName("id")
    @PrimaryKey val id: Int,
    @SerializedName("odgovoreno")
    val odgovoreno: Int,
    val idPitanja: Int,
    val idKviza: Int
)