package ba.etf.rma21.projekat.data.repositories

import android.annotation.SuppressLint
import android.content.Context
import ba.etf.rma21.projekat.data.api.OdgovorApi
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.response.OdgovorRequest
import ba.etf.rma21.projekat.data.response.OdgovorResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@SuppressLint("StaticFieldLeak")
object OdgovorRepository {
    lateinit var _context: Context
    fun setContext(context: Context) {
        _context = context
    }

    suspend fun getOdgovoriKviz(idKviza: Int): List<OdgovorResponse> {
        return ApiConfig.dajOdgovoriApi().getOdgovoriKviz(AccountRepository.acHash, idKviza)
    }

    suspend fun postaviOdgovorKviz(
        idKvizTaken: Int,
        idPitanje: Int,
        odgovor: Int
    ): OdgovorResponse {
        return ApiConfig.dajOdgovoriApi()
            .postaviOdgovorKviz(AccountRepository.acHash, idKvizTaken, OdgovorRequest(0, 0, 0))
    }


}
