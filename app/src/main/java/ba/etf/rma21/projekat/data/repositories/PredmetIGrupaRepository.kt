package ba.etf.rma21.projekat.data.repositories

import android.annotation.SuppressLint
import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.response.DodajUGrupuResponse


@SuppressLint("StaticFieldLeak")
object PredmetIGrupaRepository {

    suspend fun getPredmeti(): List<Predmet> {
        return AppDatabase.getInstance(_context).predmetDao().dajSvePredmete()
    }

    suspend fun getGrupe(): List<Grupa> {
        return AppDatabase.getInstance(_context).grupaDao().dajSveGrupe()
    }

    suspend fun getGrupeZaPredmet(idPredmeta: Int): List<Grupa> {
        val naziv = AppDatabase.getInstance(_context).predmetDao().dajPredmet(idPredmeta).naziv
        return AppDatabase.getInstance(_context).grupaDao().dajSveGrupe().filter {
            it.nazivPredmeta == naziv
        }
    }

    suspend fun upisiUGrupu(idGrupa: Int): DodajUGrupuResponse {
        return ApiConfig.dajGrupeApi().dodajStudentaUGrupu(idGrupa, AccountRepository.acHash)
    }


    suspend fun getUpisaneGrupe(): List<Grupa> {
        return AppDatabase.getInstance(_context).grupaDao().dajSveGrupe().filter {
            it.idStudenta != -1
        }
    }

    lateinit var _context: Context

    fun setContext(context: Context) {
        _context = context
    }

}