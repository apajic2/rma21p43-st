package ba.etf.rma21.projekat.data.repositories

import android.annotation.SuppressLint
import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.*
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("StaticFieldLeak")
object DBRepository {

    lateinit var _context: Context
    fun setContext(context: Context) {
        _context = context
    }

    val kvizApi by lazy {
        ApiConfig.dajKvizApi()
    }
    val predmetiApi by lazy {
        ApiConfig.dajPredmetiApi()
    }
    val grupeApi by lazy {
        ApiConfig.dajGrupeApi()
    }
    val updateApi by lazy {
        ApiConfig.dajUpdateAPi()
    }
    val accountApi by lazy {
        ApiConfig.dajAccountApi()
    }
    val pitanjaApi by lazy {
        ApiConfig.dajPitanjaApi()
    }


    val kvizDao by lazy {
        AppDatabase.getInstance(_context).kvizDao()
    }
    val predmetiDao by lazy {
        AppDatabase.getInstance(_context).predmetDao()
    }
    val grupeDao by lazy {
        AppDatabase.getInstance(_context).grupaDao()
    }
    val accountDao by lazy {
        AppDatabase.getInstance(_context).accountDao()
    }
    val pitanjaDao by lazy {
        AppDatabase.getInstance(_context).pitanjaDao()
    }

    suspend fun updateNow(): Boolean {
        val c: Date = Calendar.getInstance().time


        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
        val formattedDate: String = df.format(c)
        val updateRespose = updateApi.dajUpdateStatus(AccountRepository.acHash, formattedDate)

        val student = accountApi.dajStudenta(AccountRepository.acHash).let { studentResponse ->
            Student(studentResponse.id, studentResponse.student, studentResponse.acHash)
        }
        accountDao.dodajStudenta(student)
        val upisaneGrupe = grupeApi.dajGrupeZaStudenta(student.hash)
        val predmeti = predmetiApi.dajSvePredmete().map {
            Predmet(it.id, it.naziv, it.godina)
        }
        predmetiDao.dodajPredmete(predmeti)
        predmeti.forEach { predmet ->
            val grupe = grupeApi.dajGrupeZaPredmet(predmet.id).map { grupaResponse ->
                if (grupaResponse in upisaneGrupe)
                    Grupa(
                        grupaResponse.id,
                        grupaResponse.naziv,
                        predmet.id,
                        predmet.naziv,
                        student.id
                    )
                else Grupa(grupaResponse.id, grupaResponse.naziv, predmet.id, predmet.naziv, -1)
            }
            grupeDao.dodajGrupe(grupe)

            grupe.forEach { grupa ->
                val kvizovi = kvizApi.dajKvizoveGrupaId(grupa.id).map { kvizResponse ->
                    pitanjaDao.dodajPitanja(pitanjaApi.dajPitanjaId(kvizResponse.id).map { pitanje ->
                        Pitanje(
                            pitanje.id,
                            pitanje.naziv,
                            pitanje.tekstPitanja,
                            pitanje.opcije.joinToString(),
                            pitanje.tacan,
                            idKviza = kvizResponse.id
                        )
                    })
                    Kviz(
                        kvizResponse.id,
                        kvizResponse.naziv,
                        predmet.naziv,
                        kvizResponse.datumPocetak ?: "",
                        kvizResponse.datumKraj ?: "",
                        kvizResponse.trajanje,
                    )

                }
                val grupaKviz = kvizovi.map { GrupaKviz(grupa.id, it.id) }
                grupeDao.dodajGrupaKviz(grupaKviz)
                kvizDao.dodajSveKvizove(kvizovi)
            }
        }

        return if (updateRespose.changed == "true") {

            true
        } else {
            false
        }
    }
}