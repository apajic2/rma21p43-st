package ba.etf.rma21.projekat.data.response

import androidx.room.Entity
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizStatus
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

data class KvizResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("naziv")
    val naziv: String,
    @SerializedName("datumPocetak")
    val datumPocetak: String? = "",
    @SerializedName("datumKraj")
    val datumKraj: String? = "",
    @SerializedName("trajanje")
    val trajanje: Int,
) {
    val nazivGrupe: String = ""
}