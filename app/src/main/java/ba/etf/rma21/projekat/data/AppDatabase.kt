package ba.etf.rma21.projekat.data

import ba.etf.rma21.projekat.data.response.Odgovor
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ba.etf.rma21.projekat.data.dao.*
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.response.Account

@Database(
    entities = [Kviz::class, Grupa::class, Predmet::class, Student::class, GrupaKviz::class, Pitanje::class, Odgovor::class, Account::class],
    version = 3,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun kvizDao(): KvizDao
    abstract fun predmetDao(): PredmetDao
    abstract fun grupaDao(): GrupaDao
    abstract fun accountDao(): AccountDao
    abstract fun pitanjaDao(): PitanjaDao
    abstract fun odgovorDao(): OdgovorDao

    companion object {
        private var INSTANCE: AppDatabase? = null
        fun setInstance(appdb:AppDatabase) {
            INSTANCE=appdb
        }
        fun getInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context,
                        AppDatabase::class.java, "RMA21DB"
                    ).build()
                }
            }
            return INSTANCE!!
        }
    }
}