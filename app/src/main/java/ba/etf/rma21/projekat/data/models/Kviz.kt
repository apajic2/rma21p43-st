package ba.etf.rma21.projekat.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*


@Entity
data class Kviz(
    @PrimaryKey var id: Int,
    val naziv: String,
    val nazivPredmeta: String,
    val datumPocetka: String,
    val datumKraj: String,
    val trajanje: Int,
)